# AVENGERCON VI

## Workshop Session

### Internet of Things (IoT) Streaming Data to Google Cloud Platform (GCP)

29 November 2021
Brent Stone, Daniel Hawthorne, Luke Maffey

Client-Side Code: https://gitlab.com/brent.stone/avengercon_2021_iot

> NOTE: As of 22NOV21 there may be issues installing the client-side code dependencies in Linux.

Workshop GCP Classroom:
https://jumpstart.qwiklabs.com/classrooms/6183

## Overview

The term **Internet of Things (IoT)** refers to the interconnection of physical devices with the global Internet. These devices are equipped with sensors and networking hardware, and each is globally identifiable. Taken together, these capabilities afford rich data about items in the physical world.

[Cloud IoT Core](https://cloud.google.com/iot-core) is a fully managed service that allows you to easily and securely connect, manage, and ingest data from millions of globally dispersed devices. The service connects IoT devices that use the standard Message Queue Telemetry Transport (MQTT) protocol to other Google Cloud data services.

Cloud IoT Core has two main components:

- A device manager for registering devices with the service, so you can then monitor and configure them.
- A protocol bridge that supports MQTT, which devices can use to connect to Google Cloud.

This AVENGERCON workshop is an expanded version of the QwikLabs "[Building an IoT Analytics Pipeline on Google Cloud](https://www.qwiklabs.com/focuses/605?parent=catalog)" lab. Instead of using a simulated device within the GCP project, this workshop will cover the practical requirements of establishing a secure connection between a local IoT device and GCP.

## What You'll Learn

In this lab, you will dive into Python Controller Area Network (CAN) protocol packages and Google Cloud's IoT services and complete the following tasks:

- Connect and manage MQTT-based devices using Cloud IoT Core (using local logs or a real IoT device like the Macchina P1 connected to a passenger vehicle)
- Ingest a stream of information from Cloud IoT Core using Cloud Pub/Sub.
- Move the data from Cloud Pub/Sub to long term storage using Cloud Dataflow.
- Access the data as either a database using BigQuery or text files in cloud storage.

## Knowledge Prerequisites

This is an **advanced level** lab. It is recommended students have at least a novice level of familiarity with using Command Line Interfaces (CLI) like bash, git, and database technology like SQL. Python programming experience is required to understand and modify the provided client side code; however, it is possible to complete this workshop without programming experience. Knowledge of the following services will help you better understand the steps and commands that you'll be writing:

- BigQuery (SQL and database management)
- Cloud Pub/Sub
- Dataflow (Apache Airflow)
- IoT

If you want to get up to speed in any of the above services, check out the following Qwiklabs:

- [BigQuery: Qwik Start - Console](https://qwiklabs.com/catalog_lab/685)
- [Weather Data in BigQuery](https://qwiklabs.com/catalog_lab/476)
- [Dataflow: Qwik Start - Templates](https://qwiklabs.com/catalog_lab/934)
- [Internet of Things: Qwik Start](https://qwiklabs.com/catalog_lab/1092)

# Student Computer Configuration Prerequisites

#### Minimum System Requirements

This lab requires the following technology is installed on the computer where you are running the client-side code:

- Internet Connection
- A modern internet web browser such as Chrome, Firefox, Edge, or Safari
- Python 3.8 or later
- Permissions to run `pip install` from the user account used
- A downloaded copy of this lab’s client side code

A quick way to check whether Python and pip are correctly installed is to run the following command from Command Line (Windows) or Terminal (UNIX):

```sh
python -V
pip -V
```

or

```sh
python3 -V
pip3 -V
```

Correct installation of Python 3.8+ will print out something similar to:

```
Python 3.9.7
```

and

```
pip 21.2.4 from /usr/local/lib/python3.9/site-packages/pip (python 3.9)
```

#### Create a local project folder and virtual environment

In a location convenient for following along with this workshop, create a new folder on your local computer or device.

In that folder, open a command prompt or terminal and create a new Python virtual environment by using the following command.

```
python -m venv myenv
```

If you receive a notification that the venv failed to create follow the instructions provided by your distro to install it.

Activate the virtual environment in Windows

```
myenv\scripts\activate
```

If you receive an error that includes `...cannot be loaded because running scripts is disables on this system.` run the following command to enable scripts `Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted` then `Y` enter.

You may want to revert this setting after the lab with `Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Restricted` then `Y` enter.

Activate the virtual environment in Mac/Linux

```
source myenv/bin/activate
```

Once activated, your terminal or command prompt will show the name of your virtual environment on the left hand side of the prompt similar to the screenshot below.

![venv example](images/venv_example.png)

**Keep this terminal window open for the remainder of the workshop.** You may type deactivate if you ever need to exit your virtual environment (such as prior to deleting it). Do NOT do that now.

Upgrade pip to ensure the latest information is available.

```
python -m pip install --upgrade pip
```

## Option 0: Clone the Repo

If you are comfortable with Git you may clone the repo `git@gitlab.com:brent.stone/avengercon_2021_iot.git`.
Continue option 1 below omitting the download step.

## Option 1: Download Workshop Client-Side Source Code

This lab uses pre-written Python code that will run on your local computer or IoT device. To download the code, use either your Git client to clone the repo or click the “Download” button near the top right corner of the landing page. 

Save the source code in the same folder where you created your virtual environment.

https://gitlab.com/brent.stone/avengercon_2021_iot

![download example](images/download_example.png)

If downloading the client code as a compressed archive, be sure to uncompress into the same parent folder containing the virtual environment’s top level folder before continuing. Your directory should look like this:

![file structure](images/file_structure.png)

Install all the requirements needed to run the source code with the following command

```sh
pip install -r requirements.txt
```

## Option 2: Download and Install the Workshop Client-Side Python Wheel

If you aren’t familiar with or interested in Python but would like to follow along with the lab, you can also download the Python wheel file under the dist folder on GitLab.

https://gitlab.com/brent.stone/avengercon_2021_iot/-/tree/main/dist

Download the file with a .whl extension by clicking on the file name and then the download button in the top right corner of the GitLab window.

![download wheel](images/download_wheel.png)

Save the wheel into the same folder where you created your virtual environment.

From the command prompt or terminal window with the virtual environment activates, type the following command to install the client code
pip install `<filename>.whl`

The install was successful if no errors are shown and **avengercon** is shown in the list of Python packages when typing pip list.

## Test Client-Side Code

To verify your environment are properly configured and you can run the client-side code, use your Command Prompt window or terminal with the virtual environment activated.

Run the following command from inside the folder where you have saved the wheel or source code folder.

```sh
python -m avengercon
```

If everything is correctly configured, you will see a prompt similar to the one below.

![test output](images/test_output.png)

## Google Cloud Setup and Requirements

### Before you click the Start Lab button

Read these instructions. Labs are timed and you cannot pause them. The timer, which starts when you click Start Lab, shows how long Google Cloud resources will be made available to you.

This hands-on lab lets you do the lab activities yourself in a real cloud environment, not in a simulation or demo environment. It does so by giving you new, temporary credentials that you use to sign in and access Google Cloud for the duration of the lab.

### What you need

To complete this lab, you need:

- Access to a standard internet browser (Chrome browser recommended).
- Time to complete the lab.

**Note:** If you already have your own personal Google Cloud account or project, do not use it for this lab.

**Note:** If you are using a Chrome OS device, open an Incognito window to run this lab.

### How to start your lab and sign in to the Google Cloud Console

1. Click the Start Lab button. If you need to pay for the lab, a pop-up opens for you to select your payment method. On the left is a panel populated with the temporary credentials that you must use for this lab.

![google console](images/google_console.png)

2. Copy the username, and then click Open Google Console. The lab spins up resources, and then opens another tab that shows the Sign in page.

![sign in](images/sign_in.png)

**Tip:** Open the tabs in separate windows, side-by-side.

If you see the **Choose an account** page, click **Use Another Account**. 

![choose_account](images/choose_account.png)

3. In the Sign in page, paste the username that you copied from the left panel. Then copy and paste the password.

**Important:** You must use the credentials from the left panel. Do not use your Google Cloud Training credentials. If you have your own Google Cloud account, do not use it for this lab (avoids incurring charges).

4. Click through the subsequent pages:

- Accept the terms and conditions.
- Do not add recovery options or two-factor authentication (because this is a temporary account).
- Do not sign up for free trials.

After a few moments, the Cloud Console opens in this tab.

Ensure that the Project Name is listed in the dropdown at the top of the screen. If not, click the drop down and select the project listed in the QwikLabs Launch Lab screen.

![project name](images/project_name.png)

**Note:** You can view the menu with a list of Google Cloud Products and Services by clicking the Navigation menu at the top-left.

![nav menu](images/nav_menu.png)

## Enable APIs

Throughout this lab you'll be using your Project ID. You may want to copy and save it now for use later.

1. In the Cloud Console, click **Navigation menu** > **APIs & Services**.

![api_services](images/api_services.png)

2. Scroll down in the list of enabled APIs, and confirm that these APIs are enabled:

- Cloud IoT API
- Cloud Pub/Sub API
- Dataflow API
- Cloud Storage
- BigQuery

3. If one or more API is not enabled, click the ENABLE APIS AND SERVICES button at the top. Search for the APIs by name and enable each API for your current project.

4. To ease access to these services, click the Navigation Menu > Pin Icon next to each of the services listed above. When complete, your Navigation Menu should look similar to the one shown below.

![pin icon](images/pin_icon.png)

## Ensure that the Dataflow API is successfully enabled

To ensure access to the necessary API, restart the connection to the Dataflow API.

1. In the Cloud Console, enter Dataflow API in the top search bar. Click on the result for Dataflow API.
2. Click Manage.
3. Click Disable API.

If asked to confirm, click Disable.

4. Click Enable.

When the API has been enabled again, the page will show the option to disable.

![shows disable](images/shows_disable.png)

## Create a Cloud Pub/Sub topic

[Cloud Pub/Sub](https://cloud.google.com/pubsub) is an asynchronous global messaging service. By decoupling senders and receivers, it allows for secure and highly available communication between independently written applications. Cloud Pub/Sub delivers low-latency, durable messaging.

In Cloud Pub/Sub, publisher applications and subscriber applications connect with one another through the use of a shared string called a topic. A publisher application creates and sends messages to a topic. Subscriber applications create a subscription to a topic to receive messages from it.

In an IoT solution built with Cloud IoT Core, device telemetry data is forwarded to a Cloud Pub/Sub topic.

To define a new Cloud Pub/Sub topic:

1. In the Cloud Console, go to **Navigation menu** > **Pub/Sub** > **Topics**.
2. Click + **CREATE TOPIC**. The **Create a topic** dialog shows you a partial URL path.

**Note:** If you see qwiklabs-resources as your project name, cancel the dialog and return to the Cloud Console. Use the menu to the right of the Google Cloud logo to select the correct project. Then return to this step.

3. Add this string as your Topic ID:

```avengercon-topic```

4. Click **CREATE TOPIC**.

## Create a registry for the IoT device

To register devices, you must create a registry for the devices. The registry is a point of control for devices.

To create a new registry:

1. In the Cloud Console, go to **Navigation menu** > **IoT Core**.
2. Click **+ CREATE REGISTRY**.
3. For **Registry ID** use `avengercon-registry`
4. For **Region** select `us-central1`
5. Select the **Cloud Pub/Sub topic** you created earlier named `avengercon-topic`

![pub sub](images/pub_sub.png)

6. Select **SHOW ADVANCED OPTIONS** above the **CREATE** button.
7. Uncheck the **HTTP** Protocol option leaving only **MQTT** selected.
8. Click **CREATE**.

## Create a Cryptographic Keypair

To allow IoT devices to connect securely to Cloud IoT Core, you must create a cryptographic keypair.

To create a new cryptographic key:

1. Open the Cloud Shell terminal by clicking the small terminal icon near the top right corner of the GCP management screen.

![cloud shell](images/cloud_shell.png)

2. A Cloud Shell terminal window will open at the bottom of the browser screen.
3. Enter these commands to create the keypair in the default directory:

```sh
openssl req -x509 -newkey rsa:2048 -keyout rsa_private.pem -nodes -out rsa_cert.pem -subj "/CN=unused"
```

4. When you type ls there should now be an `rsa_cert.pem` and `rsa_private.pem` files listed in the working directory.

5. Download these files to your local computer by typing the following command

```sh
cloudshell download rsa_cert.pem rsa_private.pem
```

- NOTE: Google chrome and other browsers may block multiple file downloads. If this happens, you may see a warning message like the one shown below. Select whatever option is needed to allow multiple files to download.

![multiple downloads](images/multiple_downloads.png)

- If you continue to have issues, try altering the command to include only one file at a time and download them separately.

6. Copy-paste the `rsa_cert.pem` and `rsa_private.pem` to the avengercon folder you save the source code or wheel and setup the virtual environment.

## Add a device to the IoT Core registry

For a device to be able to connect to Cloud IoT Core, it must first be added to the registry.

To create a new device in the IoT Core registry:

1. In the Cloud Console, go to **Navigation menu** > **IoT Core**.
2. Click the avengercon-registry name under Registry ID to open the registry details.

![registry id](images/registry_id.png)

3. Selected Devices on the left hand menu then **+CREATE A DEVICE** in the top center of the window that was opened.

![create device](images/create_device.png)

4. For Device ID use the name avengercon-device.
5. Click to expand the **COMMUNICATION, CLOUD LOGGING, AUTHENTICATION** sub menu.
6. Under **Authentication (optional)** select Upload for the Input Method.
7. Select RS-256_X509 as the Public key format.
8. Click **BROWSE** and upload the `rsa_cert.pem` file created in **CLOUD SHELL** earlier in the workshop and downloaded to your local computer.
9. Click **CREATE**.
10. You should now see avengercon-device listed under the Devices view of IoT Core

![devices view](images/devices_view.png)

## Test run secure connection from local device to Google Cloud Platform Pub/Sub

You are now ready to do an initial test of your secure MQTT connection from your local device or computer to Google Cloud Platform. Before proceeding, ensure your command prompt or terminal is open and the following is true:

1. Your virtual environment is activated.
2. All source code dependencies were able to be installed.
3. Running python -m avengercon results in a descriptive help prompt.
rsa_private.pem was saved in the current folder or you’re comfortable typing the path where it is stored.
4. You have a stable internet connection.

From your configured command prompt, run the following command replacing `<your_project_id>` with the name shown at the top of your GCP console browser window.

```sh
python -m avengercon --project_id=<your_project_id> --cloud_region=us-central1 --registry_id=avengercon-registry --device_id=avengercon-device --private_key_file=rsa_private.pem --max_messages=1000 --algorithm=RS256 --mqtt_bridge_port=443
```

6. You will see `Publishing payload {“....”}` messages appear.
7. In the Cloud Console, go to **Navigation menu** > **Pub/Sub**.
8. Click the link for avengercon-topic.

![avengercon topic](images/avengercon_topic.png)

9. Click **MESSAGES** at the bottom of the middle window.
10. Select `avengercon-topic-sub` from the dropdown menu.
11. Click **PULL**.

![pull messages](images/pull_messages.png)

12. If everything is configured correctly, you should see approximately 1000 messages in the Pub/Sub topic.
13. Click the small dropdown menu icon on the right hand side of one message near `Deadline exceeded`.
14. Note the **Body JSON keys** listed in the message. This is important information we will use later in the workshop to establish a matching database table schema in BigQuery.

![json keys](images/json_keys.png)

![stop sign](images/stop_sign.png)

If you are not seeing messages in your Pub/Sub topic, it’s critical to stop and ask the instructor for assistance. All remaining steps in the workshop require successful delivery of messages from your local device or computer to Pub/Sub.

## Create a BigQuery dataset

[BigQuery](https://cloud.google.com/bigquery) is a serverless data warehouse. Tables in BigQuery are organized into datasets. In this lab, messages published into Pub/Sub will be aggregated and stored in BigQuery.

To create a new BigQuery dataset:

1. In the Cloud Console, go to **Navigation** menu > **BigQuery**.
2. Click **Done**.
3. To create a dataset, click on the **View actions** icon next to your Project ID and then select **Create dataset**.

![create dataset](images/create_dataset.png)

4. Name the dataset **avengercon_dataset**, leave all the other fields the way they are, and click **Create dataset**.
5. You should see your newly created dataset under your project:

![created dataset](images/created_dataset.png)

6. To create a table, click on the **View actions** icon next to the **avengercon_dataset** dataset and select **Open**.

![open dataset](images/open_dataset.png)

7. Now, click **+ CREATE TABLE**.

![create table](images/create_table.png)

8. Ensure that the source field is set to Empty table.
9. In the Destination section's Table name field, enter sensordata.
10. In the Schema section, click the + Add field button and add the following fields:
- **session**, set the field's **Type** to **TIMESTAMP**. Set **Mode** to **REQUIRED**.
- **id**, set the field's **Type** to **STRING**. Set **Mode** to **REQUIRED**.
- **dlc**, set the field's **Type** to **INTEGER**. Set **Mode** to **REQUIRED**.
- **time**, set the field's **Type** to **FLOAT**. Set **Mode** to **REQUIRED**.
- **data**, set the field's **Type** to **STRING**. Set **Mode** to **NULLABLE**.
11. Leave the other defaults unmodified. Click **Create table**.

## Create a cloud storage bucket and folder

[Cloud Storage](https://cloud.google.com/storage) allows world-wide storage and retrieval of any amount of data at any time. You can use Cloud Storage for a range of scenarios including serving website content, storing data for archival and disaster recovery, or distributing large data objects to users via direct download.

For this lab Cloud Storage will provide working space for your Cloud Dataflow pipeline.

1. In the Cloud Console, go to **Navigation menu** > **Cloud Storage**.
2. Click **CREATE BUCKET**.
3. For **Name**, use your Project ID then add -bucket.

- NOTE: you can copy-paste your project ID from the QwikLabs launch page or by clicking the project name at the top of the GCP management window.

4. For **Location type**, click **Multi-region** if it is not already selected.
5. For **Location**, choose the selection closest to you.
6. Click **CREATE**.
7. In the new bucket details window that shows up, click **CREATE FOLDER**.
8. Make the Name of your new folder avengercon-folder. 
9. Click **CREATE**.
10. Repeat the process to **CREATE FOLDER** but this time, the folder name should be temp.
11. Your new bucket and folders should look something like this.

![bucket structure](images/bucket_structure.png)

## Set up a Cloud Dataflow Pipeline to BigQuery

[Cloud Dataflow](https://cloud.google.com/dataflow) is a serverless way to carry out data analysis. In this lab, you will set up a streaming data pipeline to read sensor data from Pub/Sub and write this out to BigQuery and cloud storage.

1. In the Cloud Console, go to **Navigation Menu** > right click **Dataflow** > open in new tab.

- Note: This new tab will be referred to as the **second tab**. The original browser window will be called the **first tab**.

![new tab](images/new_tab.png)

2. In the top menu bar of the second tab, click **+ CREATE JOB FROM TEMPLATE**.
3. In the job-creation dialog, for **Job name**, enter `avengercon-bigquery-flow`.
4. For **Regional Endpoint**, choose the region as **us-central1**.
5. For **Dataflow template**, choose **Pub/Sub Topic to BigQuery**. When you choose this template, the form updates to review new fields below.
6. In the **second tab**, go to **Navigation Menu** > **Pub/Sub**.
7. Click the small **Copy to clipboard** icon listed next to the `avengercon-topic`.

![copy topic](images/copy_topic.png)

8. In the first tab, For Input **Pub/Sub** topic, paste the topic name copied from the **second tab**. The resulting string will look like this: `projects/qwiklabs-gcp-01-14978fb0d594/topics/avengercon-topic`
9. In the first tab, go to **Navigation Menu** > **BigQuery** > `avengercon_table` > Click the 3 dots > **Open** > **DETAILS**.

![open details](images/open_details.png)

10. The **BigQuery output table** name will be listed as the Table ID. Copy that value and go back to the second tab where we are creating the Dataflow from a template. The correct string will look like this: `qwiklabs-gcp-01-14978fb0d594:avengercon_dataset.avengercon_table`
11. Go back to the first tab. Go to **Navigation Menu** > **Cloud Storage** and click the name of the `<project_id>-bucket` we set up earlier.

![project bucket](images/project_bucket.png)

12. Click the name of the `temp` folder we set up in the bucket.

![temp folder](images/temp_folder.png)

13. Click the small **Copy the path to the clipboard icon**.

![copy path](images/copy_path.png)

14. In the **second tab**, for **Temporary location**, enter `gs://` followed by your Cloud Storage bucket name (should be able to paste the path copied from the first tab). The resulting string will look like this: `gs://qwiklabs-gcp-01-14978fb0d594-bucket/temp`
15. Click **SHOW OPTIONAL PARAMETERS**.
16. For Max workers, enter **2**.
17. For Machine type, enter **n1-standard-1**.
18. Your Dataflow creation screen should look similar to the screenshot below.

![dataflow_screen](images/dataflow_screen.png)

19. Click **RUN JOB**.

A new streaming job has started. You can now see a visual representation of the data pipeline.

NOTE: If you get an error, ensure you followed the steps listed in the section titled **Ensure that the Dataflow API is successfully enabled** to disable and re-enable the Dataflow API. Once complete, go back to the `avengercon-bigquery-flow`, click the name, select **CLONE**, then **RUN JOB**.

If you’re still getting errors, go back and double check you copy-pasted directly from the **first tab** each of the required fields and added `gs://` to the begging of the cloud storage folder path.

If you click the back arrow next to the name `avengercon-bigquery-flow` your dataflow summary may look like this.

![dataflow_summary](images/dataflow_summary.png)

## Set up a Cloud Dataflow Pipeline to Cloud Storage

1. In the Cloud Console, go to **Navigation Menu** > right click *Dataflow* > open in new tab. It is not necessary to open a third tab if you already had two open from the previous section.

- Note: This new tab will be referred to as the **second tab**. The original browser window will be called the **first tab**.

![new tab](images/new_tab.png)

2. In the top menu bar of the second tab, click **+ CREATE JOB FROM TEMPLATE**.
3. In the job-creation dialog, for Job name, enter `avengercon-storage-flow`.
4. For **Regional Endpoint**, choose the region as **us-central1**.
5. For **Dataflow template**, choose **Pub/Sub to text Files on Cloud Storage**. When you choose this template, the form updates to review new fields below.
6. In the **second tab**, go to **Navigation Menu** > **Pub/Sub**.
7. Click the small **Copy to clipboard** icon listed next to the `avengercon-topic`.

![copy topic](images/copy_topic.png)

8. In the first tab, For Input Pub/Sub topic, paste the topic name copied from the second tab. The resulting string will look like this: projects/qwiklabs-gcp-01-14978fb0d594/topics/avengercon-topic
9. In the second tab, go to Navigation Menu > Cloud Storage.
10. Click the link for the bucket created earlier in the workshop.

![bucket link](images/bucket_link.png)

11. Click the `avengercon-folder` created earlier in the workshop.

![avengercon folder](images/avengercon-folder)

12. Click the small **Copy the path to the clipboard** icon.

![copy path](images/copy_path2.png)

13. In the first tab, in the Output file directory in Cloud Storage field type `gs://` then paste the path to the `avengercon-folder`. The output string should look similar to this: `gs://qwiklabs-gcp-01-14978fb0d594-bucket/avengercon-folder`
14. For Output filename prefix type `avengercon-`.
15. In the **second tab**, click the back arrow to go back to the Cloud Storage bucket.
16. Click on the temp folder and copy its path.
17. In the **first tab**, in **Temporary location** enter `gs://` then paste the path to the temp folder. The output string should look similar to this: `gs://qwiklabs-gcp-01-14978fb0d594-bucket/temp`.
18. The Create job from template screen in the first tab should look similar to the screenshot below.

![create job](images/create_job.png)

19. Click **RUN JOB**.

## Putting it all together

You have assembled a complete pipeline and we’re ready to test that data securely moves data from our local device to both a cloud database and text files. Before running this final test, ensure both **Jobs** in **Dataflow** have a status of **Running**.

![jobs running](images/jobs_running.png)

From your configured local command prompt, run the following command replacing `<your_project_id>` with the name shown at the top of your GCP console browser window.

```sh
python -m avengercon --project_id=<your_project_id> --cloud_region=us-central1 --registry_id=avengercon-registry --device_id=avengercon-device --private_key_file=rsa_private.pem --max_messages=1000 --algorithm=RS256 --mqtt_bridge_port=443
```

### Verify BigQuery is receiving the data

1. In the Cloud Console, open the **Navigation menu** and select **BigQuery**.
2. Expand the project, then **avengercon_dataset**, then click the **3 dots** next to `avengercon_table` and click **Open**.

![open table](images/open_table.png)

3. Click the **QUERY** button in the middle screen.

![query button](images/query_button.png)

4. In the menu that opens on the right hand side of the browser, type a `*` between **SELECT** and **FROM** then click **RUN**.

![run query](images/run_query.png)

5. You will see the data from your local device displayed in the Query results windows in the bottom right corner of the browser.

![query result](images/query_results.png)

### Verify Cloud Storage is receiving the data

1. In the Cloud Console, open the **Navigation menu** and select **Cloud Storage**.
2. Select the bucket name then `avengercon-folder`.
3. You should see text files starting with `avengercon-` saved to the folder.
4. You may open those files to see that they contain the same information stored in the **BigQuery** database.

## Wrapping Up

**Congratulations!**

You successfully setup a secure pipeline for streaming local data to Google Cloud Platform using production quality tools capable of sending thousands of messages a second.

Before closing all your browsers, be sure to click Stop Lab on the jumpstart.qwiklabs.com launch page you started at. 

No other actions are required to close your personalized student account. It will automatically be deleted when you click Stop Lab or the timer expires.

Thank you for participating in this workshop. If you find any errors or have suggestions, please open an issue on the GitLab repo at https://gitlab.com/brent.stone/avengercon_2021_iot.
