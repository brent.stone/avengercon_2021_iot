=================
AvengerCon VI Workshop: Internet of Things (IoT) to Cloud Data Science
=================
This repository provides examples and references for the IoT to Cloud Data Science workshop presented at the AVENGERCON VI conference held on 29 and 30 November 2021.

For questions or concerns, please contact Brent by email:
brent [at] stoneguardsoftware [dot] com

=================
Environment Setup
=================
**Minimum System Requirements**

This lab requires the following technology is installed on the computer where you are running the client-side code:

- **Internet Connection**
- **A modern internet web browser such as Chrome, Firefox, Edge, or Safari**
- **Python 3.8 or later**
- **Permissions to run pip install from the user account used**
- **A downloaded copy of this lab's client side code**

A quick way to check whether Python and pip are correctly installed is to run the following command from Command Line (Windows) or Terminal (UNIX):

::

    python -V
    pip -V

or

::

    python3 -V
    pip3 -V

Correct installation of Python 3.8+ will print out something similar to:

::

    >>> Python 3.9.7
    >>> pip 21.2.4 from /usr/local/lib/python3.9/site-packages/pip (python 3.9)

**Create a local project folder and virtual environment**

In a location convenient for following along with this workshop, create a new folder on your local computer or device.

In that folder, open a command prompt or terminal and create a new Python virtual environment by using the following command.

::

    python -m venv myenv

You may need to :code:`apt install venv` for example :code:`sudo apt install python3.8-venv` replace 3.8 with your python version.

Activate the virtual environment in Windows/Linux

::

    myenv\scripts\activate

Activate the virtual environment in Mac

::

    source myenv\bin\activate


.. warning::
    If you receive an error that includes

    ::

        ...cannot be loaded because running scripts is disables on this system.

    run the following command to enable scripts

    ::

        Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted

    then Y enter.

    You may want to revert this setting after the lab with

    ::

        Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Restricted

    then Y enter.

Once activated, your terminal or command prompt will show the name of your virtual environment on the left hand side of the prompt similar to :code:`(venv) W:avengercon_2021_iot`.

**Keep this terminal window open for the remainder of the workshop**. You may type :code:`deactivate` if you ever need to exit your virtual environment (such as prior to deleting it). Do **NOT** do that now.

Upgrade pip to ensure the latest information is available.

::

    python -m pip install --upgrade pip


=================
References and Citations
=================
This workshop is based heavily  on the Google Cloud Platform's (GCP) `Cloud IoT Core`_ `MQTT bridge python example`_ and the `IoT Analytics Pipeline QwikLab module`_. The key difference is this workshop includes the critical plumbing needed to securely
connect a local IoT device (or emulated device) to GCP's services using an MQTT bridge and Java Web Tokens (JWT). It also provides
a very brief explanation of accessing data from a Jupyter notebook.

The conference `specific directions are available in this markdown doc`_. Please note that doc won't include the more user friendly presentation available on QwikLabs and is only available to registered developers.

Thank you to the Google Cloud Platform (GCP) team for sponsoring a private QwikLab classroom for the conference attendees.

While note explicitly needed, the physical IoT device used to collect data from Brent's car during the workshop was the `Macchina P1`_. The example data is from a 2021 Prius III.

.. _Cloud IoT Core: https://cloud.google.com/iot/docs
.. _MQTT bridge python example: https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/iot/api-client/mqtt_example
.. _IoT Analytics Pipeline QwikLab module: https://www.qwiklabs.com/focuses/605?parent=catalog
.. _Macchina P1: https://www.macchina.cc/catalog/p1-boards/p1-under-dash
.. _specific directions are available in this markdown doc: avengercon.md

=================
Additional Notes
=================
If you'd like to build the wheel from source, please `see the directions here`_.

.. _see the directions here: https://packaging.python.org/tutorials/packaging-projects/

NOTE: If you are using windows and experience a problem installing the python-can module, you may need
to manually compile and install the extension based on the PDCurses library called `windows-curses`_
.. _windows-curses: https://github.com/zephyrproject-rtos/windows-curses

When building compiling that extension, you may also need to first install the base curses library.

Compiled wheels for the curses module are available at `Christoph Gohlke's compiled wheels page`_
.. _Christoph Gohlke's compiled wheels page: _https://www.lfd.uci.edu/~gohlke/pythonlibs/#curses

Also note that installing the python-can module beyond 2.2.1 can be difficult on windows. Thus, the requirements.txt
is set to 2.2.1 despite 3.4.4 being published as of November 2021.

