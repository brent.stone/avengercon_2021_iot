#!/usr/bin/env python3
"""
Some basic unittests to ensure development and portability are in a working state.
"""
import unittest
import logging
from avengercon.data import IsoTpReader

logging.basicConfig(level=logging.INFO)


class TestSampleLogIngest(unittest.TestCase):
    def test_sample_ingest(self):
        reader = IsoTpReader()
        reader.process()


if __name__ == '__main__':
    unittest.main()
