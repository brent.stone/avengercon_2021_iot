import json
import time

data = bytearray(b'\x00\x00\x00\x80\xcc')
payload = json.dumps({'session': 1,
                      'id': "0xAA",
                      'dlc': 120,
                      'time': time.time(),
                      'data': data.hex()})
print(payload)