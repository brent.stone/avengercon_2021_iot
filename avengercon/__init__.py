#!/usr/bin/env python3
r"""Sample device that consumes configuration from Google Cloud IoT.
This example represents a stream of Controller Area Network (CAN) data from
a real device like the Macchina P1 connected to a vehicle's On-Board Diagnostics
(OBD-II) port or an emulated device like reading from a Linux SocketCAN candump
log file.

The real or emulated device publishes to Google Cloud Platform (GCP) at 'line speed'
using the Cloud IoT Core's MQTT bride. The MQTT bridge publishes the JSON to Cloud
Pub/Sub. An Apache Airflow Directed Acyclic Graph (DAG) then transports the data
from Pub/Sub to Cloud BigQuery for long term storage and follow on analysis.

To connect the device you must have downloaded Google's CA root certificates,
and a copy of your private key file. See cloud.google.com/iot for instructions
on how to do this. Run this script with the corresponding algorithm flag.

  $ python cloudiot_pubsub_example_mqtt_device.py \
      --project_id=my-project-id \
      --registry_id=example-my-registry-id \
      --device_id=my-device-id \
      --private_key_file=rsa_private.pem \
      --algorithm=RS256

With a single server, you can run multiple instances of the device with
different device ids, and the server will distinguish them. Try creating a few
devices and running them all at the same time.
"""

import argparse
import datetime
import json
import os
import ssl
import time
from importlib import resources

import jwt
import paho.mqtt.client as mqtt

from avengercon.data import IsoTpReader, initialise_real_sniffer


def create_jwt(project_id, private_key_file, algorithm):
    """Create a JWT (https://jwt.io) to establish an MQTT connection."""
    token = {
        'iat': datetime.datetime.utcnow(),
        'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=60),
        'aud': project_id
    }
    with open(private_key_file, 'r') as f:
        private_key = f.read()
    print('Creating JWT using {} from private key file {}'.format(
        algorithm, private_key_file))
    return jwt.encode(token, private_key, algorithm=algorithm)


def get_ca_root() -> str:
    """
    Function to get default roots.pem file path from the module's keys
    submodule.
    """
    with resources.path("avengercon.keys", "roots.pem") as a_ca_root_path:
        return str(a_ca_root_path)


def error_str(rc):
    """Convert a Paho error to a human readable string."""
    return '{}: {}'.format(rc, mqtt.error_string(rc))


def parse_command_line_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='Example Google Cloud IoT MQTT device connection code.')
    parser.add_argument(
        '--project_id',
        default=os.environ.get("GOOGLE_CLOUD_PROJECT"),
        required=True,
        help='GCP cloud project name.')
    parser.add_argument(
        '--registry_id',
        required=True,
        help='Cloud IoT registry id')
    parser.add_argument(
        '--device_id',
        required=True,
        help='Cloud IoT device id')
    parser.add_argument(
        '--private_key_file',
        required=True,
        help='Path to private key file.')
    parser.add_argument(
        '--algorithm',
        choices=('RS256', 'ES256'),
        required=True,
        help='Which encryption algorithm to use to generate the JWT.')
    parser.add_argument(
        '--cloud_region',
        default='us-central1',
        help='GCP cloud region')
    parser.add_argument(
        '--ca_certs',
        default=get_ca_root(),
        help='CA root certificate. Get from https://pki.google.com/roots.pem')
    parser.add_argument(
        '--max_messages',
        type=int,
        default=1000,
        help='Maximum number of messages to publish from log or real interface.')
    parser.add_argument(
        '--capture_duration',
        type=int,
        default=15,
        help='Number of seconds to sniff real interface and publish data.')
    parser.add_argument(
        '--mqtt_bridge_hostname',
        default='mqtt.googleapis.com',
        help='MQTT bridge hostname.')
    parser.add_argument(
        '--mqtt_bridge_port',
        type=int,
        default=443,  # 8883
        help='MQTT bridge port.')
    parser.add_argument(
        '--data_source', choices=('log', 'device'),
        default='log',
        help='Indicates whether the messages to be published are from '
             'a log file or a real (Linux) CAN interface.')
    parser.add_argument(
        '--log_file',
        default='',
        help='Path to the SocketCAN candump log file to read from. Only '
             'used if --data-source is log'
    )
    parser.add_argument(
        '--silent',
        type=bool,
        default=False,
        help='Silent true means mute local terminal print updates about the'
             'data flow. False (default) will show local terminal messages.'
    )

    return parser.parse_args()


def main():
    args = parse_command_line_args()

    # Create the MQTT client and connect to Cloud IoT.
    client = mqtt.Client(
        client_id='projects/{}/locations/{}/registries/{}/devices/{}'.format(
            args.project_id,
            args.cloud_region,
            args.registry_id,
            args.device_id))

    client.username_pw_set(
        username='unused',
        password=create_jwt(
            args.project_id,
            args.private_key_file,
            args.algorithm))

    client.tls_set(ca_certs=args.ca_certs, tls_version=ssl.PROTOCOL_TLSv1_2)

    client.connect(args.mqtt_bridge_hostname, args.mqtt_bridge_port)

    client.loop_start()

    # This is the topic that the device will publish telemetry events
    # (temperature data) to.
    mqtt_telemetry_topic = '/devices/{}/events'.format(args.device_id)

    # This is the topic that the device will receive configuration updates on.
    mqtt_config_topic = '/devices/{}/config'.format(args.device_id)

    # Subscribe to the config topic.
    client.subscribe(mqtt_config_topic, qos=1)

    start_time = time.time()

    if args.data_source == "log":
        log_reader = IsoTpReader(mqtt_client=client,
                                 mqtt_telemetry_topic=mqtt_telemetry_topic,
                                 file_name=args.log_file,
                                 max_messages=args.max_messages,
                                 silent=args.silent)
        log_reader.process()
    else:
        # use a real CAN interface. LINUX ONLY!!!!
        bus = initialise_real_sniffer()
        msg_count: int = 0
        for msg in bus:
            if time.time() - start_time > args.capture_duration:
                print("%d second collection window is complete. Exiting!" % args.capture_duration)
                break

            if msg_count != 0 and msg_count > args.max_messages:
                print(f"{args.max_messages} messages collected. Exiting!")
                break

            # package the CAN frame as JSON
            # https://python-can.readthedocs.io/en/master/message.html
            payload = json.dumps({'session': 1,
                                  'id': msg.arbitration_id,
                                  'dlc': msg.dlc,
                                  'time': msg.timestamp,
                                  'data': msg.data.hex()})
            client.publish(mqtt_telemetry_topic, payload, qos=1)
            if not args.silent:
                print('Publishing payload', payload)

    client.disconnect()
    client.loop_stop()
    print('Finished streaming. Goodbye!')

if __name__ == '__main__':
    main()
