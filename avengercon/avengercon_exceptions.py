#!/usr/bin/env python3
"""Custom errors and exceptions used throughout the package"""


class PayloadValueError(Exception):
    """Custom Exception for payload values which exceed the standard hexadecimal range 0-F"""


class EmptyFile(Exception):
    """Customer Exception for when the initial read from a candump file fails"""


class EmptyMessageList(ValueError):
    """A message list generated from the pre-processor was empty"""


class LogFileNotFound(Exception):
    """Raised when an input log file path doesn't appear to point to an existing file"""
