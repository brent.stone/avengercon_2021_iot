#!/usr/bin/env python3
"""
Wrap Can and IsoTP libraries to ingest candump files and attempt automatic handling of ISO-TP payloads

Alternatively, use_real_sniffer() attempts to establish a live CAN interface using Linux's SocketCAN
library. NOTE: This function is exclusive to Linux distros and likely will not work on another OS.
"""
import logging
from sys import exc_info
from typing import Tuple, Dict
import binascii
from time import time
import os
from importlib import resources
import json

import paho.mqtt.client as mqtt

from can import CanutilsLogReader, Message, rc
from can.interface import Bus, BusABC
import isotp
from isotp.errors import IsoTpError, InvalidCanDataError, ReceptionInterruptedWithFirstFrameError, \
    ReceptionInterruptedWithSingleFrameError, UnexpectedConsecutiveFrameError

from avengercon.avengercon_exceptions import EmptyFile, LogFileNotFound


def initialise_real_sniffer():
    """
    NOTE: This function is exclusive to Linux distros and likely will not work on another OS.
    """
    os.system('sudo ip link set can0 down')
    os.system('sudo ip link set can0 up type can bitrate 500000')

    rc['interface'] = 'socketcan'
    rc['channel'] = 'can0'
    rc['bitrate'] = 500000
    bus = Bus()
    print('CAN0 Setup Complete')
    return bus


class CanStack(isotp.TransportLayer):
    """
    This is a rewrite of the CanStack from the can-isotp library to better serve our needs. Essentially CanStack is
    how we track messages using a particular (Arb ID, channel) combination.
    This is thin wrapper for the isotp.protocol.TransportLayer state machine.

    Called by get_canstack() if a canstack_dict key hasn't been seen before.
    We need to dynamically differentiate whether IDs use ISO-TP. The isotp library's Address functionality always
    expects ISO-TP behavior. Thus, we need to manually manage errors and 'addresses' to handle non-ISO-TP data.
    """
    __slots__ = 'uses_isotp', 'uses_non_isotp', 'non_isotp_messages', 'isotp_messages', 'n', \
                'temp_msg_queue'

    bit_mode29 = isotp.AddressingMode.Normal_29bits
    bit_mode11 = isotp.AddressingMode.Normal_11bits

    def __init__(self, msg: Message, reader):
        """
        Use a can.Message object and IsoTpReader object to initialilize this CanStack's isotp.Address
        and isotp.TransportLayer. Both are discarded afterwards.
        :param msg: can.Message object with the Arb ID this CanStack should use for its rx address.
        :param reader: IsoTpReader object used to pass on rxfn, txfn, error_handler methods to TransportLayer
        """
        self.bus = Bus(msg.channel, bustype='virtual')  # necessary to create an isotp.protocol.TransportLayer
        if not isinstance(self.bus, BusABC):
            raise ValueError('bus must be a python-can BusABC object')

        # Toggle the isotp.protocol.TransportLayer behavior in CanStack
        if msg.is_extended_id:
            l_addressing_mode = CanStack.bit_mode29
        else:
            l_addressing_mode = CanStack.bit_mode11
        # isotp.protocol.TransportLayer requires an address to "listen" to. Generate a placeholder.
        # A ValueError is thrown if rxid == txid. This simple check prevents the error.
        if msg.arbitration_id == 0x0:
            txid = 0x1
        else:
            txid = 0x0
        addr = isotp.Address(addressing_mode=l_addressing_mode, rxid=msg.arbitration_id,
                             txid=txid, target_address=None, source_address=None, address_extension=None)
        # TODO: Update to use super.__init__()
        isotp.TransportLayer.__init__(self, rxfn=reader.rx_canbus, txfn=reader.tx_canbus, address=addr,
                                      error_handler=reader.my_error_handler)

        # This is an independent record keeping mechanism to recover from incorrectly assuming previous messages were
        # ISO-TP frames.
        self.temp_msg_queue: list = []

    def reset_stack(self):
        """reset CanStack object state"""
        self.reset()
        self.temp_msg_queue = []


def message_key(msg: Message, l_isotp: bool = False) -> Tuple[str, int, str, bool]:
    """
    Single point for updating how the CanStack.non_isotp_messages and CanStack.isotp_messages dictionaries are keyed.
    :param msg: A can.Message object created from reading in a line from a candump file
    :type msg: can.Message
    :param l_isotp: A flag to record whether messages originated from ISO-TP payloads or not. This affects the
    interpretation the leading bytes in CAN payloads which would otherwise be considered data.
    :type l_isotp: bool
    :return: a key tuple (Arb ID: int, DLC: int, Channel: str, isotp: bool).
    :rtype: tuple
    """
    return hex(msg.arbitration_id), msg.dlc, msg.channel, l_isotp


def canstack_key(msg: Message) -> Tuple[int, str]:
    """
    Single point for updating how the interim CanStack dictionaries are keyed. Because of ISO-TP DLC fluctuations and
    the possibility of non-ISO-TP payloads appearing like ISO-TP, a less strict key on ID and Channel is required.
    :param msg: A can.Message object created from reading in a line from a candump file
    :type msg: can.Message
    :return: a key tuple (Arb ID: int, Channel: str).
    :rtype: tuple
    """
    return msg.arbitration_id, msg.channel


class IsoTpReader:
    """
    Wraps the python-can and can-isotp libraries to read CAN messages from a .log Logging File (candump -L). In the
    process, it partitions the log into a nested pair of dictionaries accounting for the potential use of the ISO-TP
    transport layer protocol (ISO-15765-2) and an Arbitration ID (Arb ID) being used for multiple distinct streams that
    use different Data Length Codes (DLC) in their CAN messages (ISO-11898).

    .. note::
        each line in a .log-format file is similar to this: (datetime) channel ID#hex_data

        ``(0.0) vcan0 001#8d00100100820100``
    """

    # __slots__ = 'file_name', 'reader', 'current_msg', 'iter_protocol', 'stop', 'canstack_dict', \
    #             'message_dict', 'reingest_target', 'reingest_isotp', 'targeted_ingest', \
    #             'reingest_target_dec'

    def __init__(self,
                 mqtt_client: mqtt.Client = None,
                 mqtt_telemetry_topic: str = "",
                 max_messages: int = 1000,
                 file_name: str = "",
                 reingest_target: str = "",
                 reingest_isotp: bool = False,
                 silent: bool = False):
        """
        :param file_name: A file path used to create a can.CanutilsLogReader object to iterate over
        a candump .log and return can.Message objects.
        :type file_name: str
        :param reingest_target: OPTIONAL: used to signal a targeting re-ingest of a specific Arb ID
        :type reingest_target: str
        :param reingest_isotp: OPTIONAL: used to signal whether reingest should use ISO-TP with the
        assumption that ISO-TP is not used.
        :type reingest_isotp: bool
        :param mqtt_client: The initialized mqtt_client to export data to GCP
        :type mqtt_client: mqtt.Client
        :param mqtt_telemetry_topic: String of topic name for MQTT bridge and pub/sub
        :type mqtt_telemetry_topic: str
        :param max_messages: Number of messages to process before exiting.
        :type max_messages: int
        """
        self.mqtt_client: mqtt.Client = mqtt_client
        self.mqtt_telemetry_topic: str = mqtt_telemetry_topic
        self.silent: bool = silent
        self.max_messages: int = max_messages
        self.messages_processed: int = 0

        if file_name:
            self.file_name: str = file_name
        else:
            # No filename provided, use the sample_candump.log in the .data submodule
            with resources.path("avengercon.data", "sample_candump.log") as a_file_path:
                self.file_name: str = str(a_file_path)
        logging.info(f"Loading candump log data from {self.file_name}")

        self.reingest_target: str = reingest_target
        self.reingest_isotp: bool = reingest_isotp
        # This boolean bypasses the need to repeatedly checking len to detect a reingest target
        if len(reingest_target) > 0:
            self.targeted_ingest: bool = True
        else:
            self.targeted_ingest: bool = False
        # This allows direct comparison to Message.arbitration_id int values
        try:
            if self.targeted_ingest:
                self.reingest_target_dec: int = int(self.reingest_target, 0)
            else:
                self.reingest_target_dec: int = -1
        except ValueError as e:
            logging.error("Reingest Target %s not a well formatted hex value" % reingest_target)

        try:
            self.reader = iter(CanutilsLogReader(self.file_name))
        except FileNotFoundError as e:
            notice: str = "No candump file found at %s. Please verify filepath." % file_name
            logging.error(notice)
            raise LogFileNotFound(notice) from e
        try:
            self.current_msg: Message = next(self.reader)
        except (StopIteration, ValueError) as e:
            raise EmptyFile("There appears to be no data in the candump file") from e

        # This toggle for .rx_canbus() helps break the infinite loop in isotp.protocol.TransportLayer.process()
        self.iter_protocol = True
        self.stop: bool = False

        self.canstack_dict = {}
        self.message_dict = {}  # ISO-TP and Regular can messages are differentiated by message_key()

    def stream_log(self, file_name):
        print("Second file loading.")
        self.stop = False
        try:
            self.reader = iter(CanutilsLogReader(file_name))
        except FileNotFoundError as e:
            notice: str = "No candump file found at %s. Please verify filepath." % file_name
            logging.error(notice)
            raise LogFileNotFound(notice) from e
        try:
            self.current_msg: Message = next(self.reader)
        except (StopIteration, ValueError) as e:
            raise EmptyFile("There appears to be no data in the candump file") from e

    def get(self):
        """
        Proxy function for iterating over a candump log file and generating can.Message objects.
        This indirectly controls isotp.protocol.TransportLayer.process() via self.rx_canbus() passed in to
        TransportLayer by CanStack.__init__().
        :return: A can.Message created from the most recent line read in from a candump log file.
        :rtype: can.Message or None
        """
        try:
            self.current_msg: Message = next(self.reader)
        except StopIteration:
            self.stop = True
            return None
        except ValueError:
            logging.error("ValueError from can.CanutilsLogReader. May be caused by incomplete trailing entry in log.")
            # TODO: determine if this soft fail is appropriate, or we want to return None to create a hard stop
        # This functions as a lock on isotp.protocol.TransportLayer.process()'s calls to self.rx_canbus()
        self.iter_protocol = True
        # Verify whether we should stop based on the max messages setting
        self.messages_processed += 1
        if self.max_messages != 0 and self.messages_processed > self.max_messages:
            self.stop = True
            print(f"{self.max_messages} messages collected. Exiting!")
        return self.current_msg

    def rx_canbus(self):
        """
        Returns an isotp.CanMessage if self.iter_protocol is set. Otherwise, None.
        isotp.protocol.TransportLayer gets this function during CanStack.__init__() to help control its
        state machine. This IsoTpReader object gets passed to .TransportLayer via CanStack.__init__() during
        self.new_canstack().
        :return: An isotp.CanMessage object that be processed by a CanStack object.
        :rtype: isotp.CanMessage or None
        """
        # isotp.protocol.TransportLayer.process() will loop calling self.rx_canbus() until None is returned.
        # self.iter_protocol ensures only one message is returned to isotp.protocol.TransportLayer.process from
        # self.rx_canbus().
        if self.iter_protocol:
            self.iter_protocol = False
            return isotp.CanMessage(arbitration_id=self.current_msg.arbitration_id,
                                    dlc=len(self.current_msg.data),
                                    data=self.current_msg.data,
                                    extended_id=self.current_msg.is_extended_id,
                                    is_fd=self.current_msg.is_fd)
        # None breaks the loop in isotp.protocol.TransportLayer.process()
        return None

    def get_canstack(self, msg: Message) -> (Tuple, CanStack):
        """
        Called by self.process_message(). Checks if this message (ID, channel) belongs to an existing CanStack. If not,
        create a new stack and save it to the self.canstack_dict. Either way, return the canstack for (ID, channel).
        :param msg: A python-can.Message created from a line of text in a candump log
        :return: tuple like (key, CanStack); CanStack objects wrap isotp.protocol.TransportLayer
        :rtype: tuple
        """
        key = canstack_key(msg=msg)
        if key not in self.canstack_dict.keys():
            self.canstack_dict[key] = CanStack(msg=msg, reader=self)
        return key, self.canstack_dict[key]

    def append_message_dict(self, msg: Message, l_isotp: bool = False) -> None:
        """
        Helper function for adding messages to the exported message dictionaries
        :param msg:
        :param l_isotp:
        :return: None. Updates happen inplace on self.message_dict
        """
        # If this is a targeted re-ingest of a specific arb ID, filter any Messages not the target
        if self.targeted_ingest and (msg.arbitration_id != self.reingest_target_dec):
            return
        key: tuple = message_key(msg, l_isotp)
        # TODO: Call a new function that streams to GCP via MQTT
        if self.mqtt_client is not None:
            payload = json.dumps({'session': 1,
                                  'id': str(key),
                                  'dlc': msg.dlc,
                                  'time': msg.timestamp,
                                  'data': msg.data.hex()})
            self.mqtt_client.publish(self.mqtt_telemetry_topic, payload, qos=1)
            if not self.silent:
                print('Publishing payload', payload)
        elif not self.silent:
            print(f"!!Not-publishing!! {msg.timestamp}\t{key}\t{msg.data}")
        # self.signalMessage.emit(key, msg)
        if key not in self.message_dict.keys():
            self.message_dict[key] = []
        self.message_dict[key].append(msg)

    def process_message(self, msg: Message) -> None:
        """
        Try to automatically infer whether this particular message is part of an ISO-TP sequence.
        Frame Types: Single Frame (SF), First Frame (FF), Consecutive Frame (CF), Flow Control (FC)

        NOTE: ISO-TP may cause DLC to fluctuate while building an extended frame. Throughout this
        function, account for the possibility of that fluctuation while maintaining the requirement
        that MessageGroup payload length must be constant going into lexical analysis.

        The following states need to be independently accounted for by each MessageGroup:

        *Possible valid ISO-TP*
        1. Apparently valid ISO-TP SF or sequence of FF followed by mix of CF and FC
            1a. Potential ISO-TP SF or valid FF, CF, FC sequence tracked by the CanStack(isotp.TransportLayer).rx_queue
                was received. Get & save the complete ISO-TP frame from canstack.rx_queue.get() then reset the canstack
            1b. canstack.RxState.WAIT_CF is True, continue to enqueue messages in .rx_queue

        2. Current message breaks ISO-TP protocol flow despite previous messages appearing to be FF, CF, FC frames.
           Assume previous messages coincidentally appeared to be ISO-TP. Also assume the queued messages and the
           current message are actually regular CAN frames. Process them as state #1 then reset the canstack.
                - NOTE: Pay special attention that each message (ID, Channel, DLC) metadata gets matched the same keyed
                canstack.

        *No ISO-TP*
        3. No previous or current indication of ISO-TP based on the leading payload bytes.
            Output: Create and store a regular can.Message immediately.

        *Bonafide Errors*
        4. There may be a bonafide error in the file. This is most likely from an incomplete final entry in the candump
           log caused by the sniffer being ungracefully stopped (e.g. power off hard shutdown or ctrl+c in terminal).

        *Targeted Re-Ingest of a Specific ArbID
        5.  We may want to force ISO-TP to be used or not used for a particular Arb ID. If so, only
            consider messages for that ID. If 'force' ISO-TP, use the state machine as usual.
            Otherwise, 'forcing' non-ISO-TP skips the state machine entirely and saves the message

        :param msg: The current can.Message object created from reading in a line from the candump file.
        :return: None. All updates happen in the CanStack returned from self.get_isotp_listener(msg). NOTE: the actual
        parsed message(s) may require updating a different CanStack canstack.
        """
        # key is a canStack key like (Arb ID: int, Channel: str)
        key, canstack = self.get_canstack(msg=msg)

        # Are we just trying to re-ingest a specific Arb ID to toggle assumed ISO-TP behavior?
        # This is originally triggered in the __init__.py GUI's tang_adjustment_panel.py
        # add_isotp_button() .connect call to .toggle_isotp()
        if self.targeted_ingest:
            # Is this the specific Arb ID we're looking for? If not, skip this entire function.
            if key[0] != self.reingest_target_dec:
                return
            # Are we assuming messages are not using ISO-TP?
            if not self.reingest_isotp:
                # Preemptively do STATE 3 - NON ISO-TP (see below for full state machine inclusion)
                self.append_message_dict(msg=msg, l_isotp=False)
                canstack.reset_stack()  # resets ISO-TP TransportLayer and canstack.temp_msg_queue
                return
            else:
                # proceed as usual and process this can.Message as a potential ISO-TP message
                pass

        try:
            # Trigger isotp.TransportLayer.process() which calls the .process_rx() and .process_tx() state
            # machine management. Both functions in turn call the self.rx_canbus() and self.tx_canbus() functions
            # passed into isotp.TransportLayer.__init__() during CanStack.__init__().
            canstack.process()
            # move completed ISO-TP messages from the queue to
            # self.message_dict: dict{(ID, DLC, Channel): [can.Message, ...]}
            # STATE 1a - Completed ISO-TP messages are ready
            if canstack.rx_queue.qsize() > 0:
                while not canstack.rx_queue.empty():
                    isotp_data: bytearray = canstack.rx_queue.get()
                    isotp_msg = Message(timestamp=msg.timestamp,
                                        # TODO: offset timestamps to prevent non-unique index?
                                        arbitration_id=msg.arbitration_id,
                                        extended_id=msg.is_extended_id,
                                        channel=msg.channel,
                                        dlc=len(isotp_data),
                                        data=isotp_data)
                    self.append_message_dict(msg=isotp_msg, l_isotp=True)
                    # self.log_metadata.update(msg=isotp_msg, isotp=True)
                canstack.temp_msg_queue = []
                return
        except (IsoTpError, ValueError) as e:
            # STATE 2 and/or 3 - Potentially regular CAN frames were mistaken as ISO-TP. Reverse the previous assumption
            # and process them regular CAN messages.
            # This arb ID may not be using ISO-TP right now, but that doesn't mean it wasn't before or will do so
            # again. This logic attempts to differentiate whether this is a genuine error or just a non iso-tp
            # message.
            if e is InvalidCanDataError or \
                    ReceptionInterruptedWithFirstFrameError or \
                    ReceptionInterruptedWithSingleFrameError or \
                    UnexpectedConsecutiveFrameError or \
                    ValueError:
                logging.debug("arb id %d on channel %s generated an expected ISO-TP error."
                              % (msg.arbitration_id, msg.channel))
                # The temp_msg_queue is recovered below the else clause.
                self.append_message_dict(msg=msg, l_isotp=False)
            # STATE 4 - Bonafide error
            else:
                # This appears to be an actual error.
                logging.error("arb id %d on channel %s raised an unhandled error %s: %s" %
                              (key[0], key[1], exc_info()[0], exc_info()[1]))
                logging.error("rx_state: %s \t tx_state: %s \t rx_queue: %s \t rx_buffer: %s \t msg: %s" %
                              (canstack.rx_state, canstack.tx_state, canstack.rx_queue.qsize(),
                               binascii.hexlify(canstack.rx_buffer), msg))
            if len(canstack.temp_msg_queue) > 0:
                # Recover messages we previously guessed were ISO-TP segments. This will incorrectly interpret
                # messages that were legitimate ISO-TP segments during a real error, but we'll take that risk.
                for message in canstack.temp_msg_queue:
                    self.append_message_dict(msg=message, l_isotp=False)
            canstack.reset_stack()  # Resets the ISO-TP state and temp_msg_queue
            return
        # STATE 1b - ISO-TP message is still being assembled
        else:
            # If we're here, the try clause didn't throw an exception or continue after finding an ISO-TP message in
            # queue. Check if we're currently building an ISO-TP message. If so, skip to the next while loop
            # iteration.
            if canstack.rx_state == canstack.RxState.WAIT_CF:
                # We're potentially waiting for more ISO-TP PDUs
                canstack.temp_msg_queue.append(msg)
                return

        # STATE 3 - Normal CAN Message
        # No exception was thrown and the following two if statements did not trigger a return:
        # if canstack.rx_queue.qsize() > 0 in the try block
        # if canstack.rx_state == canstack.RxState.WAIT_CF in the else block
        # Thus, msg is likely to be a regular CAN message. Save it and reset the canstack.
        self.append_message_dict(msg=msg, l_isotp=False)
        # self.log_metadata.update(msg, isotp=False)
        canstack.reset_stack()  # resets ISO-TP TransportLayer and canstack.temp_msg_queue

    def process(self) -> (Dict, Dict):
        """
        This is the primary function of this class that iterates over the candump log.
        :return: tuple of (dict{key: (ID, Channel, DLC), val: list[can.Message]}, self.log_metadata).
        self.log_metadata: ingest.log_metadata.LogMetadata
        :rtype: Tuple(dict, LogMetadata)
        """
        start_time = time()
        while True:
            msg: Message = self.get()
            if msg is None or self.stop:
                break
            # Don't bother trying to process messages that were errors or remote frames with no data.
            if msg.is_error_frame or msg.is_remote_frame:
                continue
            self.process_message(msg)
        # self.log_metadata.finalize()
        logging.info("Time to ingest and preprocess ISO-TP payloads from file: %s" % str(time() - start_time))

        return self.message_dict

    @staticmethod
    def tx_canbus(msg) -> None:
        """
        Since we're only reading a file, tx is a dummy function to facilitate the isotp.protocol.TransportLayer FSM to
        progress through its states during the second half of .process()
        :param msg: can.Message
        :return: None
        """

    @staticmethod
    def my_error_handler(this_error: IsoTpError):
        """
        ISO-TP and non ISO-TP messages may appear in candump logs. Errors from the isotp.protocol.TransportLayer class
        can be used to guess whether normal messages coincidentally used leading bytes that match ISO-TP control data.
        Thus, this error handler is a pass-through function to access TransportLayer's errors via CanStack.
        :param this_error:
        :type this_error: isotp.errors.IsoTpError
        :return: The same error received
        :rtype: isotp.errors.IsoTpError
        """
        # logging.debug('IsoTp layer error occurred: %s - %s' % (type(this_error).__name__, str(this_error)))
        logging.debug('IsoTp layer error occurred: %s - %s' % (exc_info()[0], exc_info()[1]))
        raise this_error
